<?php

    require_once("masterpage/header.php");

?>
   <br>
    <div class="container" id="quienes">
        <br>
        <div class="row">
            <div class="col-md-6 text-center">
            <h6 class="display-6 text-center"><strong>HISTORIA</strong></h6>
                <p style="text-align:justify; font-size:20px;">
                Nuestra empresa surge con la finalidad de servir a la comunidad en el campo de la salud, de ahí de que nuestras puertas se abrieran para ofrecer nuestros servicios a la población en general el día 26 de junio del 2010 y tiene origen en la ciudad de Puruàndiro Michoacán de Ocampo.

                <p style="text-align:justify; font-size:20px;"> Somos una empresa con un enfoque humano, permitiéndonos aportar nuestros conocimientos profesionales en el procesamiento de análisis clínicos y microbiológicos basados en la ética profesional que nos caracteriza otorgando confiabilidad y respaldo en la calidad de resultados emitidos; nos destacamos por empatizar con la economía de nuestros pacientes proporcionando precios accesibles en forma personalizada.   </p>

                <p style="text-align:justify; font-size:20px;">En Laboratorios Biosalud contamos con equipos y materiales de la más alta calidad en las diferentes áreas de trabajo, así mismo nos preocupamos por contar en nuestras filas personal calificado que practique los valores humanos y de ética profesional que garantizan una atención de calidad y confiabilidad al prestar un servicio.

                De igual manera reiteramos el compromiso con cada uno de nuestros pacientes al contribuir con nuestros conocimientos en su calidad de vida.
                </p>
                
            </div>
            <div class="col-md-6">
                 <img class="border border-dark" 
                    style="" src="public/img/historia.jpg" id="img_history" alt="">
            </div>
        </div>
         
    </div>

    <br><br>
<!--DIVIDER-->
<div class="container-fluid" style="background-color:#cf73acd3; width: 100%; height: 10px;">
      <div class="col-md-12">
        <span style="visibility: hidden;">1</span>
      </div>
</div>
<!--END DIVIDER-->
<br><br>
<div class="container text-center px-4" id="efectos_history">
    <div class="row gx-5">
        <div class="col-md-4 p-3" id="misionvisionvalores01" >
            <img src="public/img/objetivo.png" style="width:20%; heigth:60px;" alt="">
            <br>
            <span style="font-size:20px;"><strong>Misión</strong></span>
            <br>
            <p style="text-align:justify;  font-size:20px;">
                Proporcionar un servicio de análisis clínico confiable y oportuno para auxiliar 
                en el diagnóstico de patologías clínicas, 
                sobre una base de ética profesional 
                y alto compromiso con la calidad.
            </p>
        </div>
        <div class="col-md-4 p-3" id="misionvisionvalores02">
                <img src="public/img/motivacion.png" style="width:20%; heigth:60px;" alt="">
                <br>
                <span style="font-size:20px;"><strong>Visión</strong></span>
                <br>
                <p style="text-align:justify;  font-size:20px;">
                    Ser un departamento que proporcione los servicios más especializados
                     y de alta calidad a médicos y pacientes,
                      convirtiendonos en el líder de la medicina de labotario clínico.
                </p>
        </div>
        <div class="col-md-4 p-3" id="misionvisionvalores03">
            <img src="public/img/alto-valor.png" style="width:20%; heigth:60px;" alt="">
                <br>
                <span style="font-size:20px;"><strong>Valores</strong></span>
                <br>
                    <ul style="text-align:justify;  font-size:20px;">
                        <p><i class="icofont-verification-check">Ética profesional</i></p>
                        <p><i class="icofont-verification-check">Calidad</i></p>
                        <p><i class="icofont-verification-check">Responsabilidad</i></p>
                        <p><i class="icofont-verification-check">Compromiso Social</i></p>
                        <p><i class="icofont-verification-check">Excelencia</i></p>
                    </ul>
        </div>
    </div>
</div>

<!--END DIVIDER-->
<br>
<!--DIVIDER-->
<div class="container-fluid" style="background-color:#cf73acd3; width: 100%; height: 10px;">
      <div class="col-md-12">
        <span style="visibility: hidden;">1</span>
      </div>
</div>
<!--END DIVIDER-->
<br><br>














<br><br><br>
<?php

    require_once("masterpage/footer.php");

?>