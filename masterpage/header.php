<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=s, initial-scale=1.0">
    <title>biosaludlaboratorios</title>
    <link rel="stylesheet" href="public/style.css">
    <link rel="stylesheet" href="public/style2.css">
    <link rel="stylesheet" href="public/bootstrap.min.css">
    <link rel="stylesheet" href="public/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="public/icofont/icofont.min.css">
    <link rel="stylesheet" href="public/js/box/dist/css/lightbox.min.css">
    <link rel="stylesheet" href="public/js/box/dist/css/lightbox.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
   
    <script src="public/js/load.js"></script>
    <script src="public/js/fade.js"></script>
    <script src="public/js/service.js"></script>
    <script src="public/js/history.js"></script>
    <script src="public/js/fade_galeria.js"></script>
    <script src="public/js/top.js"></script>
    <script src="public/js/fecha.js"></script>  
</head>
<body id="body">

<div id="loader">
  <img src="public/img/load.gif" alt="">

</div>



  <button onclick="topFunction()" id="myBtn" title="Go to top"><strong>IR AL MENÚ</strong></button>

  
<div class="container text-center m-2">
    <div class="row">
        <div class="col-md-6" style="font-size:18px;">
            <span><i class="fa-solid fa-envelope"></i><strong> contacto@biosaludlaboratorios.com.mx</strong> </span>
            <span><i class="fa-solid fa-phone"></i><strong> (438) 383 2472 </strong> </span>
            <span><i class="fa-solid fa-phone"></i><strong> (438) 383 1010 </strong> </span>
        </div>
        
        <div class="col-md-6" style="font-size:20px;">
           <a href="#" target="_blank"> <i class="fa-brands fa-whatsapp"></i></a> 
           <a href="#" target="_blank"> <span><i class="fa-brands fa-instagram"></i></span></a> 
           <a href="#" target="_blank"> <span><i class="fa-brands fa-facebook"></i> </span></a>
        </div>
    </div>

</div>
    <!--NAV BAR-->

<div class="container-fluid"  style="font-size:15px;" id="div_carousel"> 

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">

  <a class="navbar-brand" href="#"><span class="text-dark"><strong><img src="public/img/logo02.png" style="  width:250px; height: 50%;"  alt=""></strong></span></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-start" id="navbarSupportedContent">
      <ul class="navbar-nav" id="underline_nav">
        <ul class="navbar-nav">
    
     
        <li class="nav-item">
          <a class="nav-link active"  href="index.php"><span class="text-dark"><i class="fa-solid fa-house"></i> <strong> Inicio</strong></span></a>
        </li>
     
        <!-- <li class="nav-item">
          <a class="nav-link active" href="#"><span class="text-dark"><strong>Tipos Cuartos</strong></span></a>
        </li>-->
        <li class="nav-item">
          <a class="nav-link active" href="quienes.php"><span class="text-dark"><i class="fa-solid fa-address-card"></i> <strong> Quiénes somos</strong></span></a>
        </li>
        <li class="nav-item">
         <a class="nav-link active" href="servicios.php"><i class="fa-solid fa-folder"></i><span class="text-dark"><strong> Servicios</strong></span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="equipo.php"><i class="fa-solid fa-people-group"></i><span class="text-dark"><strong> Equipo de trabajo</strong></span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="galeria.php"><i class="fa-solid fa-images"></i><span class="text-dark"><strong> Galería</strong></span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="ubicacion.php"><i class="fa-solid fa-location-dot"></i><span class="text-dark"><strong> Ubicación</strong></span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="certificado.php"><i class="fa-solid fa-file"></i><span class="text-dark"><strong> Calidad</strong></span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="contacto.php"><i class="fa-regular fa-book-journal-whills"></i><span class="text-dark"><strong> Agenda tu Cita</strong></span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="Buzon.php"><i class="fa-regular fa-book-journal-whills"></i><span class="text-dark"><strong> Buzón de sugerencias </strong></span></a>
        </li>
       
        <!--<li class="nav-item">
          <a class="nav-link disabled">Disabled</a>
        </li>
      </ul>-->
     <!-- <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
          </form>
    -->
    </div>
  </div>
</nav>

</div>
 
<!--end navbar-->


<!--REDES FIXED-->

<div id="redes">
  <div class="d-grid">
    <div class="p-1"><a href="" target="_blank"><span><img src="public/img/facebook.png" id="img_follow" alt=""></span></a></div>
    <div class="p-1"><a href="" target="_blank"><span><img src="public/img/instagram.png" id="img_follow" alt=""></span></a></div>
    <div class="p-1"><a href="" target="_blank"><span><img src="public/img/whatsapp.png" id="img_follow" alt=""></span></a></div>
  </div>
</div>
<!--REDES FIXED-->
    
    