    <div class="container m-2 text-center">
        <div class="row">
            <div class="col-12">
                <a href="admin/login.php"><h6 style="visibility: hidden;">ADMIN</h6></a> 
            </div>
        </div>
    </div>
    <div class="container-fluid text-center" style="font-size:20px;" >
        <div class="row">
            <div class="col-md-4">
                <span><i class="fa-solid fa-envelope"></i><strong> contacto@biosaludlaboratorios.com.mx</strong> </span>
              
            </div>
            <div class="col-md-4">
                 <a href=""><img src="public/img/logobio.png"  alt=""></a>
            </div>
            <div class="col-md-4">
                <a href="#" target="_blank"> <i class="fa-brands fa-whatsapp"></i></a> 
                <a href="#" target="_blank"> <span><i class="fa-brands fa-instagram"></i></span></a> 
                <a href="#" target="_blank"> <span><i class="fa-brands fa-facebook"></i> </span></a>
            </div>
        </div>
    </div>

    <div class="container-fluid text-center" style="font-size:20px;">
        <div class="row">
            <div class="col">
           |     © Copyright BioSalud. All Rights Reserved
             <h6> Desarrollado por <a href="https://engineerweb.com.mx/" target="_blank">Engineer Web</a></h6>
            </div>
        </div>
    </div>
    
    
    <script src="public/js/box/dist/js/lightbox-plus-jquery.min.js"></script>
    <script src="public/js/box/dist/js/lightbox-plus-jquery.js"></script>
    <script src="public/js/box/dist/js/lightbox.min.js"></script>
    <script src="public/js/box/dist/js/lightbox.js"></script>
    <link rel="stylesheet" href="public/style.css">
    <link rel="stylesheet" href="public/bootstrap.min.css">
    <link rel="stylesheet" href="public/fontawesome/css/all.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <br>
</body>
</html>