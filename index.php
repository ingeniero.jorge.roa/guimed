<?php

    require_once("masterpage/header.php");

?>
    
 <!--CAROUSEL-->   
<div class="container-fluid" style="box-sizing: border-box;">

<div id="carouselExampleIndicators" id="mycarousel"  class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="4" aria-label="Slide 5"></button>
  </div>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="public/img/slide01.png" class="d-block w-100"  alt="... "  data-bs-interval="1000">
    </div>
    <div class="carousel-item">
      <img src="public/img/slide02.png" class="d-block w-100" alt="..." data-bs-interval="2000">
    </div>
    <div class="carousel-item">
      <img src="public/img/3.png" class="d-block w-100"  alt="..." data-bs-interval="2000">
    </div>
    <div class="carousel-item">
      <img src="public/img/4.png" class="d-block w-100"  alt="..." data-bs-interval="2000">
    </div>
    <div class="carousel-item">
      <img src="public/img/5.png" class="d-block w-100"  alt="..." data-bs-interval="1000">
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>

</div>

<!--END CAROUSEL-->
<br>
<!--DIVIDER-->
<div class="container-fluid" style="background-color:#cf73acd3; width: 100%; height: 10px;">
      <div class="col-md-12">
        <span style="visibility: hidden;">1</span>
      </div>
</div>
<!--END DIVIDER-->
<br>

<div class="container text-center">
      <p class="text-center display-6">RECUERDA QUE CONTAMOS CON SERVICIO A DOMICILIO</p>
      <img src="public/img/SERVICIO.webp" alt="" id="img_service">
    <div class="container text-center m-2">
      <a href="contacto.php"><button class=" fs-5 btn_contact" ><strong>Clic para agendar cita</strong> </button></a>
    </div> 
</div>


<br>
<br>
<!--DIVIDER-->
<div class="container-fluid" style="background-color:#cf73acd3; width: 100%; height: 10px;">
      <div class="col-md-12">
        <span style="visibility: hidden;">1</span>
      </div>
</div>
<!--END DIVIDER-->
<br>
  
<div class="container"  id="div_seccion02">
<h3 class="text-center m-2 display-5"><strong> LABORATORIOS <span style="color:#DD06AC;">BIO</span>SALUD </strong></h3>
    <div class="row">
        <div class="col-md-6" >
           

            <p class="fs-3">En Laboratorios BioSalud podrán encontrar una opción que se ajuste a sus necesidades de diagnóstico clinico, 
              con equipo tecnológico y atención que son sometidos constantemente a un proceso de mejora continua, ya que contamos con bases firmes, 
              para brindar un servicio de calidad. Fuimos creados con el propósito de llevar servicios de prevención 
              y diagnóstico a la población mexicana, a precios accesibles y ofreciéndoles una atención y equipo de calidad.</p>
        </div>
        <div class="col-md-6">
        <iframe width="100%" height="370" src="https://www.youtube.com/embed/FQPjNs_z7-M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            
        </div>
    </div>
      <div class="container text-center m-2">
            <button class=" fs-5 btn_contact" ><strong>Clic para más información</strong> </button>
      </div> 
      <br>       
</div>
<br><br>
<!--DIVIDER-->
<div class="container-fluid" style="background-color:#cf73acd3; width: 100%; height: 10px;">
      <div class="col-md-12">
        <span style="visibility: hidden;">1</span>
      </div>
</div>
<!--END DIVIDER-->
<br><br>


<!--Seccion services-->
    <div class="container" id="services">
         <div class="row">
            <div class="col-md-12">
                <h6 class="text-center m-2 display-5" id="underline_service">ALGUNOS DE NUESTROS SERVICIOS</h6>
                <p class="text-dark text-center fs-3">
                  Aplicamos las más exigentes normas de calidad en todas las fases del proceso, desde: la recepción, el registro, la toma, el traslado de la muestra, su procesamiento por personal competente y con equipos de tecnología de vanguardia, hasta la emisión del resultado.
                </p>
            </div>
            <div class="col-md-6 text-center" id="service_01">
                <span><i class="icofont-laboratory"></i></span>
                <h6 class="text-dark fs-3">QUIMICAS SANGUINEA</h6>
                <p class="text-dark fs-5">
                Laboratorio Biosalud se preocupa por brindarte un servicio de calidad con equipo a la medida.
                </p>
                <div class="container text-center m-2">
                  <a href="servicios.php"><button class="fs-5 btn_contact" ><strong>Clic para ver más servicios</strong> </button></a>
               </div> 
               <br>
            </div>
            <div class="col-md-6 text-center" id="service_01">
                <span><i class="icofont-microscope-alt"></i></span>
                <h6 class="text-dark fs-3">HEMATOLOGIA</h6>
                <p class="text-dark fs-5">
                Contamos con el mejor equipo de trabajo para tus estudios.<br>
                </p>
                <br>
                <div class="container text-center m-2">
                 <a href="servicios.php"><button class="fs-5 btn_contact" ><strong>Clic para ver más servicios</strong> </button></a>
               </div>
               <br>
            </div>
         </div>      
    </div>
<!--End services-->

<!--END DIVIDER-->
<br>
<!--DIVIDER-->
<div class="container-fluid" style="background-color:#cf73acd3; width: 100%; height: 10px;">
      <div class="col-md-12">
        <span style="visibility: hidden;">1</span>
      </div>
</div>
<!--END DIVIDER-->
<br><br>
<!--FOLLOW IN SOCIAL-->

  <div class="container">
    <div class="row">
      <h6 class="display-5 text-center"><STRONG>
          SÍGUENOS Y COMUNÍCATE CON NOSOTROS A TRAVÉS DE NUESTRAS REDES SOCIALES
          </STRONG></h6>
        <div class="col-md-12 text-center">
          <a href="" target="_blank"><span><img src="public/img/facebook.png" id="img_follow" alt=""></span></a>
          <a href="" target="_blank"><span><img src="public/img/instagram.png" id="img_follow" alt=""></span></a>
          <a href="" target="_blank"><span><img src="public/img/whatsapp.png" id="img_follow" alt=""></span></a>
        </div>  
      </div> 
   </div>

  <br>
<!--DIVIDER-->
<div class="container-fluid" style="background-color:#cf73acd3; width: 100%; height: 10px;">
      <div class="col-md-12">
        <span style="visibility: hidden;">1</span>
      </div>
</div>
<!--END DIVIDER-->
<br><br>


<!--END FOLLOR IN SOCIA-->

<?php

    require_once("masterpage/footer.php");

?>