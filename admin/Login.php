<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../public/style2.css">
    <link rel="stylesheet" href="../public/bootstrap.min.css">
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
    <link rel="stylesheet" href="../public/icofont/1icofont.min.css">
    <title>LOGIN</title>
</head>
<body class="animate__animated animate__bounce" style="background-color: #cf73acd3;">

    
            <div class="container m-2" style="background-color: white;
                                              border-radius: 20px;
                                              width: 50%;
                                              position: relative;
                                              left:25%;
                                              top:200px;"
                                                >

                <div class="row">
                    <div class="col-md-12">
                        <form action="verificacionusuario.php" method="post">

                            <br>
                            <div class="container text-center m-2"><h6><strong>INICIO DE SESIÓN</strong></h6></div>
                            <br>
                            <div class="container m-2">
                                <span class="input-group-text" ><i class="icofont-ui-user"></i></span> 
                                <input class="form-control"  type="text" name="usuario" placeholder="user" required>
                            </div>
                            <div class="container m-2">
                                <span class="input-group-text" ><i class="icofont-ui-password"></i></span> 
                                <input class="form-control "  type="password" name="contraseña" placeholder="password" required>
                            </div>
                            <div class="text-center">
                               <input type="submit" class="btn btn-info m-1 text-center" value="ACCEDER">
                            </div>
                        </form>
                    </div>
                </div>

            </div> 
    


</body>
</html>
    