<?php

    require_once("masterpage/header.php");

?>

<div class="container-fluid bg-dark">
    <p class="text-center" style="color:white; font-size:25px;  font-family: 'Times New Roman', Times, serif;">
        Para visualizar de una mejor manera las imágenes da clic sobre ellas.
    </p>
</div>

<br>

<div class="container text-center">
    <br>
    <div class="row">
        <div class="col-md-3">
            <a class="example-image-link" href="public/img/01.jpg" data-lightbox="example-1"><img id="img_galeria" class="example-image" src="public/img/01.jpg" alt="image-1" /></a> 
         </div>
        <div class="col-md-3">
            <a class="example-image-link" href="public/img/02.jpg" data-lightbox="example-1"><img  id="img_galeria" class="example-image" src="public/img/02.jpg" alt="image-1" /></a> 
        </div>
        <div class="col-md-3">
            <a class="example-image-link" href="public/img/03.jpg" data-lightbox="example-1"><img id="img_galeria" class="example-image" src="public/img/03.jpg" alt="image-1" /></a> 
        </div>

      <div class="col-md-3">
          <a class="example-image-link" href="public/img/04.jpg" data-lightbox="example-1"><img id="img_galeria" class="example-image" src="public/img/04.jpg" alt="image-1" /></a> 
      </div>
        <br>
    </div>
</div>
<br>
<div class="container text-center">
    <div class="row">
        <div class="col-md-3" >
             <a class="example-image-link" href="public/img/05.jpg" data-lightbox="example-1"><img id="img_galeria" class="example-image" src="public/img/05.jpg" alt="image-1" /></a> 
        </div>
        <div class="col-md-3">
            <a class="example-image-link" href="public/img/06.jpg" data-lightbox="example-1"><img id="img_galeria" class="example-image" src="public/img/06.jpg" alt="image-1" /></a> 
        </div>
        <div class="col-md-3">
         <a class="example-image-link" href="public/img/07.jpg" data-lightbox="example-1"><img id="img_galeria" class="example-image" src="public/img/07.jpg" alt="image-1" /></a> 
         </div>
        <div class="col-md-3">
            <a class="example-image-link" href="public/img/08.jpg" data-lightbox="example-1"><img id="img_galeria" class="example-image" src="public/img/08.jpg" alt="image-1" /></a> 
        </div>
        </div>
</div>
<br>
<div class="container text-center" id="fade_galeria">
    <div class="row">
        <div class="col-md-4" >
             <a class="example-image-link" href="public/img/09.jpg" data-lightbox="example-1"><img id="img_galeria" class="example-image" src="public/img/09.jpg" alt="image-1" /></a> 
        </div>
        <div class="col-md-4">
            <a class="example-image-link" href="public/img/10.jpg" data-lightbox="example-1"><img id="img_galeria" class="example-image" src="public/img/10.jpg" alt="image-1" /></a> 
        </div>
        <div class="col-md-4">
         <a class="example-image-link" href="public/img/11.jpg" data-lightbox="example-1"><img id="img_galeria" class="example-image" src="public/img/11.jpg" alt="image-1" /></a> 
         </div>
         
        </div>
</div>
<div class="container text-center" id="fade_galeria">
    <div class="row">
        <div class="col-md-5" >
         <a class="example-image-link" href="public/img/12.jpg" data-lightbox="example-1"><img id="img_galeria" class="example-image" src="public/img/12.jpg" alt="image-1" /></a> 
         </div>
         <div class="col-md-5" >
         <a class="example-image-link" href="public/img/13.jpg" data-lightbox="example-1"><img id="img_galeria" class="example-image" src="public/img/13.jpg" alt="image-1" /></a> 
         </div>
         <div class="col-md-5" >
         <a class="example-image-link" href="public/img/14.jpg" data-lightbox="example-1"><img id="img_galeria" class="example-image" src="public/img/14.jpg" alt="image-1" /></a> 
         </div>
        </div>
</div>
<br><br>

































<script>
    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    })
</script>

<?php

    require_once("masterpage/footer.php");

?>