<?php

    require_once("masterpage/header.php");

?>


    <div class="container" id="">
        <div class="row">
          <div class="col-md-12 m-2">
             <h6 class="text-center text-dark display-3"
              id="underline_service"><strong>"NUESTROS SERVICIOS"</strong></h6> 
          </div>
          <div class="col-md-12 text-center">
            <span><i class="icofont-laboratory"></i></span>
            <p class="m-2 display-6">Bioquímica <span><strong>Clínica</strong> </span></p> 
            <p class="m-2" style="font-size:25px;">Equipos de vanguardia</p> 
            <P class="m-2" style="font-size:20px;"><strong>RAPIDEZ, PRECISIÓN, CONTROL DE CALIDAD: ESTUDIOS DE CONFIABILIDAD</strong></P>
            <ul style="list-style:none">
              <li style="font-size:20px;">Química Clínica</li>
              <li style="font-size:20px;">Perfil de lípidos</li>
              <li style="font-size:20px;">Electrolitos</li>
              <li style="font-size:20px;">Pruebas de funcionamiento Hepático y renal</li>
              <li style="font-size:20px;">Enzimas</li>
              <li style="font-size:20px;">Drogas de abuso</li>
            </ul>
          </div>
          <div class="col-6 text-center" id="service_01">
                <span><i class="icofont-laboratory"></i></span>
                <h6 class="text-dark fs-3">HEMATOLOGIA</h6>
                <p class="text-dark fs-5">
                  Biometría hemática  grupo y RH fórmula blanca.  Fórmula roja plaquetas velocidad de sedimentación reticulocitos.
                </p>
                <div class="container text-center m-2">
                  <a href="contacto.php"><button class="fs-5 btn_contact" ><strong>Clic para agendar tu cita</strong> </button></a> 
               </div> 
               <br>
          </div>
          <div class="col-6 text-center" id="service_01">
                <span><i class="icofont-laboratory"></i></span>
                <h6 class="text-dark fs-3">QUIMICAS SANGUINEA</h6>
                <p class="text-dark fs-5">
                  Glucosa urea creatinina ácido úrico colesterol triglicéridos  perfil de lípidos pruebas de función hepática amilasa lipasa.  Cpk. Ck mb
                </p>
                <div class="container text-center m-2">
                    <a href="contacto.php"><button class="fs-5 btn_contact" ><strong>Clic para agendar tu cita</strong> </button></a> 
               </div> 
               <br>
          </div>
        </div>
    </div>

  <br>
    
    <div class="container" id="">
        <div class="row">
          <div class="col-6 text-center" id="service_01">
                <span><i class="icofont-laboratory"></i></span>
                <h6 class="text-dark fs-3">COAGULACIÓN</h6>
                <p class="text-dark fs-5">
                tp.  Tpt. INR  fibrinogeno dinero D.
                </p>
                <div class="container text-center m-2">
                   <a href="contacto.php"><button class="fs-5 btn_contact" ><strong>Clic para agendar tu cita</strong> </button></a> 
               </div> 
               <br>
          </div>
          <div class="col-6 text-center" id="service_01">
                <span><i class="icofont-laboratory"></i></span>
                <h6 class="text-dark fs-3">ELECTROLITOS</h6>
                <p class="text-dark fs-5">
                  Sodio potasio cloro calcio fósforo magnesio.
                </p>
                <div class="container text-center m-2">
                   <a href="contacto.php"><button class="fs-5 btn_contact" ><strong>Clic para agendar tu cita</strong> </button></a> 
               </div> 
               <br>
          </div>
        </div>
    </div>
  <br>
    <div class="container" id="">
        <div class="row">
          <div class="col-6 text-center" id="service_01">
                <span><i class="icofont-laboratory"></i></span>
                <h6 class="text-dark fs-3">INMUNOLOGÍA </h6>
                <p class="text-dark fs-5">
                Vdrl.    Proteína C reactiva Factor reumatoide. Antiestreptolisinas   . Reacciones febriles   Hepatitis A Hepatitis B Hepatitis C    Coombs Directo. Coombs indirecto VIH.
                </p>
                <div class="container text-center m-2">
                  <a href="contacto.php"><button class="fs-5 btn_contact" ><strong>Clic para agendar tu cita</strong> </button></a> 
               </div> 
               <br>
          </div>
          <div class="col-6 text-center" id="service_01">
                <span><i class="icofont-laboratory"></i></span>
                <h6 class="text-dark fs-3">ORINA</h6>
                <p class="text-dark fs-5">
                Examen general de orina.
                </p>
                <div class="container text-center m-2">
                   <a href="contacto.php"><button class="fs-5 btn_contact" ><strong>Clic para agendar tu cita</strong> </button></a> 
               </div> 
               <br>
          </div>
        </div>
    </div>
<!--END DIVIDER-->
<br>
    <div class="container" id="">
        <div class="row">
          <div class="col-6 text-center" id="service_01">
                <span><i class="icofont-laboratory"></i></span>
                <h6 class="text-dark fs-3">COPROLOGIA </h6>
                <p class="text-dark fs-5">
                Coproparasitoscopico 1,2,3    Coprologico.  Citología de moco fecal. sangre oculta en heces.  Amiba en fresco Azúcares reductores
                </p>
                <div class="container text-center m-2">
                 <a href="contacto.php"><button class="fs-5 btn_contact" ><strong>Clic para agendar tu cita</strong> </button></a> 
               </div> 
               <br>
          </div>
          <div class="col-6 text-center" id="service_01">
                <span><i class="icofont-laboratory"></i></span>
                <h6 class="text-dark fs-3">PRUEBAS ESPECIALES</h6>
                <p class="text-dark fs-5">
                Perfil ginecólogo.  Perfil tiroideo.                        HGC cuantitativa      Vitamina D.   Insulina  Tiroglobulina    Ca 125.  Ca 15.3 Ca 19.9   Ah. Carcinoembrionario. Alfa feto proteína Antígeno prostático.
                </p>
                <div class="container text-center m-2">
                 <a href="contacto.php"><button class="fs-5 btn_contact" ><strong>Clic para agendar tu cita</strong> </button></a> 
               </div> 
               <br>
          </div>
        </div>
    </div>

    <br>
    <div class="container" id="">
        <div class="row">
          <div class="col-6 text-center" id="service_01">
                <span><i class="icofont-laboratory"></i></span>
                <h6 class="text-dark fs-3">MICROBIOLOGÍA </h6>
                <p class="text-dark fs-5">
                 Urocultivo. Coprocultivo  Hemocultivo Baciloscopia. Cultivo secreción de herida.  Etc.
                </p>
                <div class="container text-center m-2">
                 <a href="contacto.php"><button class="fs-5 btn_contact" ><strong>Clic para agendar tu cita</strong> </button></a> 
               </div> 
               <br>
          </div>
          <div class="col-6 text-center" id="service_01">
                <span><i class="icofont-laboratory"></i></span>
                <h6 class="text-dark fs-3">PRUEBAS COVID 19</h6>
                <p class="text-dark fs-5">
                Prueba Rapida  Antígeno COVID ., Anticuerpos COVID cualitativo y cuantitativo prueba COVID 19 (PCR).
                </p>
                <div class="container text-center m-2">
                 <a href="contacto.php"><button class="fs-5 btn_contact" ><strong>Clic para agendar tu cita</strong> </button></a> 
               </div> 
               <br>
          </div>
        </div>
    </div>
<!--END DIVIDER-->
<br>
<!--DIVIDER-->
<div class="container-fluid" style="background-color:#cf73acd3; width: 100%; height: 10px;">
      <div class="col-md-12">
        <span style="visibility: hidden;">1</span>
      </div>
</div>
<!--END DIVIDER-->
<br><br>





















  

<?php

    require_once("masterpage/footer.php");

?>