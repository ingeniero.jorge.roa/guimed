<?php 
    require_once("masterpage/header.php");
?>

    <div class="container border border-dark">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center m-3" style="font-family: 'Times New Roman', Times, serif;"><strong>CERTIFICACIÓN POR PACAL</strong> </h3>
                <p style="text-align:justify; font-size:25px; font-family: 'Times New Roman', Times, serif;" class="m-3 text-center">
                    En laboratorios <span style="color:#DD06AC;">Bio</span>salud contamos con una evaluación de control de Calidad externo.
                </p>
                <h4 class="text-center m-3" style="font-family: 'Times New Roman', Times, serif;"><strong>¿QUÉ ES LA CERTIFICACIÓN PACAL?</strong> </h4>
                <P class="m-3" style="text-align:justify; font-size:25px; font-family: 'Times New Roman', Times, serif;">
                    PACAL, una empresa 100% Mexicana:
                    CERTIFICADA INTERNACIONALMENTE CON ISO 9001:2015 Y ACREDITADA NACIONALMENTE ANTE LA EMA, Entidad Mexicana de Acreditación en la ISO/IEC 17043:2010


                    PACAL evalúa, desde hace 30 años mensualmente a sus usuarios, a través de un
                    Programa de Evaluación Externa de la Calidad a más de 3,500 laboratorios públicos, privados y sociales a nivel nacional.
                    Este Programa de Evaluación Externa de la Calidad permite a todos los laboratorios inscritos, mejorar día a día su Calidad Analítica, asegurando a la Salud de la Población Mexicana.

                </P>
                <a href="https://www.pacal.org/" target="_blank"><p class="text-center"><img src="public/img/pacal.jpg" alt=""></p></a>
            </div>
        </div>
    </div>
    <br>











<?php 
    require_once("masterpage/footer.php");
?>