<?php 
    require_once("masterpage/header.php");
?>


    <div class="container bg-dark" >
        <div class="row">
            <div class="col-md-12">
                 <h5 class="m-3 text-center" style="color:white;"><strong>UBICACIÓN Y CONTACTO</strong> </h5>
                 <h4 class="m-3 text-center" style="color:white;"><strong>DIRECCIÓN AV. INDPENDENCIA No 385, PURUÁNDIRO MICHOACÁN</strong> </h4>
                 <h4 class="m-3 text-center" style="color:white;">
                    <span><i class="fa-solid fa-envelope"></i><strong> contacto@biosaludlaboratorios.com.mx</strong> </span>
                    <span><i class="fa-solid fa-phone"></i><strong> (438) 383 2472 </strong> </span>
                    <span><i class="fa-solid fa-phone"></i><strong> (438) 383 1010 </strong> </span>
                 </h4>
            </div>
            <div class="col-md-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3747.0815551205046!2d-101.52127328525327!3d20.088877386514945!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842c51d6aaa75d9b%3A0x334368563e4bfc45!2sLaboratorios%20Biosalud!5e0!3m2!1ses-419!2smx!4v1651263057265!5m2!1ses-419!2smx" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
    </div>
    <br>
    <div class="container bg-dark" >
        <div class="row">
            <div class="col-md-12">
                 <h5 class="m-3 text-center" style="color:white;"><strong>UBICACIÓN Y CONTACTO</strong> </h5>
                 <h4 class="m-3 text-center" style="color:white;"><strong>DIRECCIÓN CALLE. JOSÉ GUADALUPE SALTO No 9, PURUÁNDIRO MICHOACÁN</strong> </h4>
                 <h4 class="m-3 text-center" style="color:white;">
                    <span><i class="fa-solid fa-envelope"></i><strong> contacto@biosaludlaboratorios.com.mx</strong> </span>
                    <span><i class="fa-solid fa-phone"></i><strong> (438) 383 2472 </strong> </span>
                    <span><i class="fa-solid fa-phone"></i><strong> (438) 383 1010 </strong> </span>
                 </h4>
            </div>
            <div class="col-md-12">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3747.1586806221217!2d-101.51726398525335!3d20.085652586516744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842c51d0f19b1785%3A0x552e782b61f951ea!2sLaboratorio%20Biosalud.!5e0!3m2!1ses!2smx!4v1651766878035!5m2!1ses!2smx" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
    </div>
    <br><br>







<?php 
    require_once("masterpage/footer.php");
?>