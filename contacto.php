﻿<?php

    require_once("masterpage/header.php");

?>

        <div class="container" id="contacto">
            <div class="row">
                <div class="col-md-6 text-center">
                    <h4><strong>LLENA LA INFORMACIÓN SOLICITADA</strong> </h4>
                     <form action="Reservacion.php" method="post">
                    <div class="mb-3">
                        <input type="text" id="btn_form" name = Nombre placeholder="INTRODUCE TU NOMBRE COMPLETO" required>
                    </div>
                    <div class="mb-3">
                        <input type="number" id="btn_form" name ="Numero" placeholder="INTRODUCE TU NÚMERO TELEFÓNICO" required>
                    </div>
                    <div class="mb-3">
                        <input type="text" id="btn_form" name ="Domicilio" placeholder="INTRODUCE TU DOMICILIO" required>
                    </div>
                    <div class="mb-3">
                    
                    <select  name="Lugar" id="btn_form" required>
                            <option value="">ELIGE EL SERVICIO A DOMICILIO O EN LABORATORIO QUE DESEAS RESERVAR</option>
                            <option value="A DOMICILIO">A DOMICILIO</option>
                            <option value="EN LABORATORIO Guadalupe Salto ">EN LABORATORIO GUADALUPE SALTO </option> 
                            <option value="EN LABORATORIO Av. Independencia ">EN LABORATORIO Av. INDEPENDENCIA</option>
                        </select>
                     </div>
                    <div class="mb-3">
                        <input type="date" id="btn_form" name="Fecha" min="2022-01-01" class="form-control" required >
                    </div>
                    <div class="mb-3">
                        <label>Horarios de atención 7:00 am a 7:30 pm </label>
                        <input type="time"  id="btn_form" name="Hora" min="7:00" max="19:15"  class="form-control" required >
                    </div>
                   
                    <div class="mb-3">
                        <select name="Servicio" id="btn_form" required>
                            <option value="">ELIGE EL SERVICIO QUE DESEAS RESERVAR</option>
                            <option value="HEMATOLOGIA">HEMATOLOGIA</option>
                            <option value="QUIMICAS SANGUINEA">QUIMICAS SANGUINEA</option>
                            <option value="COAGULACIÓN">COAGULACIÓN</option>
                            <option value="ELECTROLITOS">ELECTROLITOS</option>
                            <option value="INMUNOLOGÍA">INMUNOLOGÍA</option>
                            <option value="ORINA">ORINA</option>
                            <option value="COPROLOGIA">COPROLOGIA</option>
                            <option value="PRUEBAS ESPECIALES">PRUEBAS ESPECIALES</option>
                            <option value="MICROBIOLOGÍA">MICROBIOLOGÍA</option>
                            <option value="ORINA">ORINA</option>
                            <option value="PRUEBAS COVID 19">PRUEBAS COVID 19</option>
                            
                        </select>
                    </div>
                    
                    <div class="mb-3 text-center ">
                        <button  type="submit" class="btn_contact_02">ENVIAR MENSAJE</button>
                    </div>
                    
                </form>
                </div>
                <div class="col-md-6 text-center" >
                     <div>
                         <h6 style="font-size:25px;"><strong>DIRECCIÓN</strong></h6>
                        <h6 style="font-size:20px;"><i class="fa-solid fa-street-view"></i><strong> Guadalupe Salto #9 Col. Centro</strong> </h6>
                        <h6 style="font-size:20px;"><i class="fa-solid fa-street-view"></i><strong> Av. Independencia #385 Col. Centro</strong> </h6>
                        <h6><strong> __________________________________</strong> </h6>
                    </div>
                    <br><br>
                    <div>
                        <h6 style="font-size:25px;"><strong>CORREO ELECTRÓNICO</strong></h6>
                        <h6 style="font-size:20px;"><i class="fa-solid fa-envelope"></i><strong> contacto@biosaludlaboratorios.com.mx</strong> </h6>
                        <h6><strong> __________________________________</strong> </h6>
                    </div>
                    <br><br>
                    <div>
                        <h6 style="font-size:25px;"><strong>NÚMEROS TELEFÓNICOS</strong></h6>
                        <h6 style="font-size:20px;"><i class="fa-solid fa-phone"></i><strong> (438) 383 2472 </strong> </h6>
                        <h6 style="font-size:20px;"><i class="fa-solid fa-phone"></i><strong> (438) 383 1010 </strong> </h6>
                        <h6><strong> __________________________________</strong> </h6>
                    </div>
                    
                   
                </div>
                <div class="col-md-12 text-center">
                       <img style="width:200px;" src="public/img/logo02.png" alt="">
                    </div>
                    
            </div>
        </div>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3747.0815551205046!2d-101.52127328525327!3d20.088877386514945!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842c51d6aaa75d9b%3A0x334368563e4bfc45!2sLaboratorios%20Biosalud!5e0!3m2!1ses-419!2smx!4v1651263057265!5m2!1ses-419!2smx" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
        </div>



   
    

<br><br><br>
<?php

    require_once("masterpage/footer.php");

?>