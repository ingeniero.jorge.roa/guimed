<?php

    require_once("masterpage/header.php");

?>

<div class="container px-4 text-center" >

    <div class="col-md-12 m-2">
             <h6 class="text-center text-dark display-6"
              id="underline_service"><strong>"EQUIPO DE TRABAJO"</strong></h6> 
          </div>
  <div class="row gx-5">
    <div class="col-md-6">
     <div class="p-3 border bg-light">
         <span><img src="public/img/julio.jpeg" id="img_equipo01"  class="img-thumbnail" alt=""></span>
         <div id="div_mover_img_01">
            <h5 class="m-2"><strong> DIRECTOR GENERAL</strong></h5>
            <h6 class="m-2">QFB  Julio Cesar herrejon Hernández </h6>
           
            <a href="#" target="_blank"> <i class="fa-brands fa-whatsapp"></i></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-instagram"></i></span></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-facebook"></i> </span></a>
         </div>
     </div>
    </div>
    <div class="col-md-6">
      <div class="p-3 border bg-light">
      <span><img src="public/img/leticia.jpeg" id="img_equipo02" class="img-thumbnail" alt=""></span>
      <div id="div_mover_img_02">
            <h5 class="m-2"><strong> DIRECTORA GENERAL</strong></h5>
            <h6>QFB. Leticia Álvarez Ibarra</h6>

            <a href="#" target="_blank"> <i class="fa-brands fa-whatsapp"></i></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-instagram"></i></span></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-facebook"></i> </span></a>
         </div>
      </div>
    </div>

    
    <div class="col-12">
      <div class="p-3 border bg-light">
        <span><img src="public/img/martha.jpeg" id="img_equipo02" class="img-thumbnail" alt=""></span>
        <div id="div_mover_img_02">
              <h5 class=""><strong>  AUXILIAR ADMINISTRATIVO</strong></h4>
              <h6>Martha Álvarez Ibarra</h6>
            <a href="#" target="_blank"> <i class="fa-brands fa-whatsapp"></i></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-instagram"></i></span></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-facebook"></i> </span></a>
         </div>
        </div>
      </div>

    <div class="col-md-12">
      <div class="p-3 border bg-light">
        <span><img src="public/img/KATYA.jpeg" id="img_equipo02" class="img-thumbnail" alt=""></span>
        <div id="div_mover_img_02">
              <h5 class=""><strong>  QUÍMICA SUPERVISORA</strong></h5>
              <h6>QFB. Katia Samantha Patiño Torres</h6>
            <a href="#" target="_blank"> <i class="fa-brands fa-whatsapp"></i></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-instagram"></i></span></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-facebook"></i> </span></a>
         </div>
        </div>
    </div>

      <div class="col-md-4">
      <div class="p-3 border bg-light">
        <span><img src="public/img/WENDY.jpeg" id="img_equipo02" class="img-thumbnail" alt=""></span>
        <div id="div_mover_img_02">
              <h5 class=""><strong>  QUÍMICA ANALISTA</strong></h5>
              <h6>QFB. Wendy Lopez Palomares</h6>
            <a href="#" target="_blank"> <i class="fa-brands fa-whatsapp"></i></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-instagram"></i></span></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-facebook"></i> </span></a>
         </div>
        </div>
      </div>
      <div class="col-md-4">
      <div class="p-3 border bg-light">
        <span><img src="public/img/luis.jpeg" id="img_equipo02" class="img-thumbnail" alt=""></span>
        <div id="div_mover_img_02">
              <h5 class=""><strong>  QUÍMICO ANALISTA</strong></h5>
              <h6>QFB. Luis Fernando Calixto Mejia</h6>
            <a href="#" target="_blank"> <i class="fa-brands fa-whatsapp"></i></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-instagram"></i></span></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-facebook"></i> </span></a>
         </div>
        </div>
      </div>
      <div class="col-md-4">
      <div class="p-3 border bg-light">
        <span><img src="public/img/yatziry.jpeg" id="img_equipo02" class="img-thumbnail" alt=""></span>
        <div id="div_mover_img_02">
              <h5 class=""><strong>  QUÍMICA ANALISTA</strong></h5>
              <h6>QFB. Yatziri Daniela Orozco Alcaraz</h6>
            <a href="#" target="_blank"> <i class="fa-brands fa-whatsapp"></i></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-instagram"></i></span></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-facebook"></i> </span></a>
         </div>
        </div>
      </div>
     
      
      <div class="col-md-4">
      <div class="p-3 border bg-light">
        <span><img src="public/img/carolina.jpeg" id="img_equipo02" class="img-thumbnail" alt=""></span>
        <div id="div_mover_img_02">
              <h5 class=""><strong>  QUÍMICA ANALISTA</strong></h5>
              <h6>QFB. Julieta Carolina García bonales</h6>
            <a href="#" target="_blank"> <i class="fa-brands fa-whatsapp"></i></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-instagram"></i></span></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-facebook"></i> </span></a>
         </div>
        </div>
      </div>
       
      <div class="col-md-4">
      <div class="p-3 border bg-light">
        <span><img src="public/img/jeny.jpeg" id="img_equipo02" class="img-thumbnail" alt=""></span>
        <div id="div_mover_img_02">
              <h5 class=""><strong>  TÉCNICA LABORATORIO CLÍNICO</strong></h5>
              <h6>TLC. Jenifer Ortiz</h6>
            <a href="#" target="_blank"> <i class="fa-brands fa-whatsapp"></i></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-instagram"></i></span></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-facebook"></i> </span></a>
         </div>
        </div>
      </div>

      <div class="col-md-4">
      <div class="p-3 border bg-light">
        <span><img src="public/img/ale.jpeg" id="img_equipo02" class="img-thumbnail" alt=""></span>
        <div id="div_mover_img_02">
              <h5 class=""><strong>  TÉCNICA LABORATORIO CLÍNICO</strong></h5>
              <h6>TLC. Alejandra Armenta</h6>
            <a href="#" target="_blank"> <i class="fa-brands fa-whatsapp"></i></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-instagram"></i></span></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-facebook"></i> </span></a>
         </div>
        </div>
      </div>




      <div class="col-md-6">
      <div class="p-3 border bg-light">
        <span><img src="public/img/almakaren.jpeg" id="img_equipo03" class="img-thumbnail" alt=""></span>
        <div id="div_mover_img_02">
              <h5 class=""><strong>  RECEPCIONISTA</strong></h4>
              <h6>Alma Karen Díaz Díaz</h6>
            <a href="#" target="_blank"> <i class="fa-brands fa-whatsapp"></i></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-instagram"></i></span></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-facebook"></i> </span></a>
         </div>
        </div>
      </div>

      
      <div class="col-md-6">
      <div class="p-3 border bg-light">
        <span><img src="public/img/clara.jpeg" id="img_equipo02" class="img-thumbnail" alt=""></span>
        <div id="div_mover_img_02">
              <h5 class=""><strong>  RECEPCIONISTA</strong></h4>
              <h6>Clara Rubi Anguiano Mejia</h6>
            <a href="#" target="_blank"> <i class="fa-brands fa-whatsapp"></i></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-instagram"></i></span></a> 
            <a href="#" target="_blank"> <span><i class="fa-brands fa-facebook"></i> </span></a>
         </div>
        </div>
      </div>

    
    
   

  </div>
</div>





<br><br><br>
<?php

    require_once("masterpage/footer.php");

?>